# jitsi-meet-build

Just a CI pipeline to build jitsi-meet electron app linux packages (`.deb`, `.rpm` and `AppImage`) from latest code.

## Install

### 🐧 Deb package

Download and install: [jitsi-meet-amd64.deb](https://gitlab.com/libreops/packages/jitsi-meet-electron/-/jobs/artifacts/master/raw/jitsi-meet-amd64.deb?job=build)

From a terminal this should be:

```
dpkg -i jitsi-meet-amd64.deb
```

### 🎩  RPM package

Download and install: [jitsi-meet-x86_64.rpm](https://gitlab.com/libreops/packages/jitsi-meet-electron/-/jobs/artifacts/master/raw/jitsi-meet-x86_64.rpm?job=build)

From a terminal this should be:

```
rpm -i jitsi-meet-x86_64.rpm
```

### 🎁 AppImage

Download and run: [jitsi-meet-x86_64.AppImage](https://gitlab.com/libreops/packages/jitsi-meet-electron/-/jobs/artifacts/master/raw/jitsi-meet-x86_64.AppImage?job=build)

You may need to change permission to the file to be executable. From a terminal this should be:

```
chmod +x jitsi-meet-x86_64.AppImage
```